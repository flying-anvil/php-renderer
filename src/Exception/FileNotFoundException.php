<?php

declare(strict_types=1);

namespace FlyingAnvil\PhpRenderer\Exception;

use Exception;

class FileNotFoundException extends Exception
{
}
